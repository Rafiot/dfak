---
layout: page
title: Postal Mail
author: mfc
language: ru
summary: Contact methods
date: 2018-09
permalink: /ru/contact-methods/postal-mail.md
parent: /ru/
published: true
---

Sending mail is a slow communication method if you are facing an urgent situation. Depending on the jurisdictions where the mail travels, the authorities may open the mail, and they often track the sender, sending location, recipient, and destination location.