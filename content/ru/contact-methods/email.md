---
layout: page
title: Email
author: mfc
language: ru
summary: Contact methods
date: 2018-09
permalink: /ru/contact-methods/email.md
parent: /ru/
published: true
---

The content of your message as well as the fact that you contacted the organization may be accessible by governments or law enforcement agencies.